package net.christianemo.chiquimusic.eureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class ChiquimusicEurekaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChiquimusicEurekaApplication.class, args);
	}

}
