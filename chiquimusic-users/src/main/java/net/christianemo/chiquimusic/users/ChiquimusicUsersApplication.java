package net.christianemo.chiquimusic.users;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class ChiquimusicUsersApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChiquimusicUsersApplication.class, args);
	}

}
