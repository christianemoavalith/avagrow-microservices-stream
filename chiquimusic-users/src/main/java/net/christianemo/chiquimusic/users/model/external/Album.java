package net.christianemo.chiquimusic.users.model.external;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;
import lombok.NonNull;

@Data
public class Album {

	private Long id;

	private String producer;

	private String artist;

	private Integer members;

	private Integer releaseYear;

	private List<Long> tracks = new ArrayList<>();

	public Integer getAmmountOfTracks() {
		return tracks.size();
	}

}
