package net.christianemo.chiquimusic.users.model;

import lombok.Data;
import net.christianemo.chiquimusic.users.model.external.Track;

@Data
public class User {

	private Long id;
	
	private final String username;
	
	private Mood mood;
	
	public boolean isRecomendable(Track track){
		return mood.isRecomendable(track);
	}
	
}
