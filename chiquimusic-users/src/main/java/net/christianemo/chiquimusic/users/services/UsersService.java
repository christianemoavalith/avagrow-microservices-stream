package net.christianemo.chiquimusic.users.services;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import net.christianemo.chiquimusic.users.errors.NotFoundException;
import net.christianemo.chiquimusic.users.errors.UnnexpectedMoodNameException;
import net.christianemo.chiquimusic.users.model.Mood;
import net.christianemo.chiquimusic.users.model.User;
import net.christianemo.chiquimusic.users.model.external.Track;

@Service
public class UsersService {

	// Simulates database in memory
	private List<User> users = new ArrayList<>();

	public User create(User user) {
		var lastId = users.stream()
				.mapToLong(u -> u.getId())
				.max()
				.orElse(0);
		var nextId = lastId + 1; // First element will always be 1L

		user.setId(nextId);
		users.add(user);

		return user;
	}

	public User read(Long userId) throws NotFoundException {
		return findUserById(userId);
	}

	public List<User> list() {
		return new ArrayList<>(users);
	}

	public User update(User user, Long userId) throws NotFoundException {
		if (!user.getId().equals(userId))
			throw new IllegalArgumentException(
					"Given user doesn't match with given id");

		var localUser = findUserById(userId);
		if (localUser == null)
			throw new NotFoundException();

		var index = users.indexOf(user);
		users.set(index, user);

		return user;
	}

	public void delete(Long userId) throws NotFoundException {
		var localUser = findUserById(userId);
		if (localUser == null)
			throw new NotFoundException();

		var index = users.indexOf(localUser);
		users.remove(index);
	}

	public void changeMoodOf(Long userId, String moodName)
			throws UnnexpectedMoodNameException, NotFoundException {
		var moodObject = Mood.createMoodFor(moodName);
		var localUser = findUserById(userId);
		localUser.setMood(moodObject);
	}

	public Boolean isTrackRecommendableForUser(Track track, Long userId)
			throws NotFoundException {
		var localUser = findUserById(userId);
		return isTrackRecommendableForUser(track, localUser);
	}

	public Boolean isTrackRecommendableForUser(Track track, User user) {
		return user.isRecomendable(track);
	}

	public List<Track> filterRecommendableForUser(List<Track> tracks, Long userId)
			throws NotFoundException {
		var localUser = findUserById(userId);
		return filterRecommendableForUser(tracks, localUser);
	}

	public List<Track> filterRecommendableForUser(List<Track> tracks, User user) {
		return new ArrayList<Track>(
				tracks.stream()
						.filter(t -> user.isRecomendable(t))
						.collect(Collectors.toList()));
	}

	private User findUserById(Long id) throws NotFoundException {
		return users.stream()
				.filter(u -> u.getId().equals(id))
				.findFirst()
				.orElseThrow(() -> new NotFoundException());
	}

}
