package net.christianemo.chiquimusic.users.controllers;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import net.christianemo.chiquimusic.users.errors.NotFoundException;
import net.christianemo.chiquimusic.users.errors.UnnexpectedMoodNameException;
import net.christianemo.chiquimusic.users.model.User;
import net.christianemo.chiquimusic.users.model.external.Track;
import net.christianemo.chiquimusic.users.services.UsersService;
import net.christianemo.chiquimusic.users.viewmodels.UserVM;

@RestController
@RequestMapping("/api/users")
public class UsersController {

	@Autowired
	private UsersService usersService;

	@PostMapping
	public User create(@RequestBody UserVM userVm) throws UnnexpectedMoodNameException {
		return usersService.create(UserVM.userOf(userVm));
	}

	@GetMapping("/{userId}")
	public UserVM read(@PathVariable Long userId) throws NotFoundException {
		var found = usersService.read(userId);
		return UserVM.of(found);
	}

	@GetMapping
	public List<UserVM> list() {
		var viewmodels = usersService.list().stream()
				.map(u -> UserVM.of(u))
				.collect(Collectors.toList());
		return viewmodels;
	}

	@PutMapping
	public UserVM update(UserVM userVm, Long userId)
			throws NotFoundException, UnnexpectedMoodNameException {

		if (!userVm.getId().equals(userId))
			throw new IllegalArgumentException("Ids don't match");

		var userObject = UserVM.userOf(userVm);
		var updated = usersService.update(userObject, userId);
		return UserVM.of(updated);
	}

	@DeleteMapping("/{userId}")
	public void delete(@PathVariable Long userId) throws NotFoundException {

		usersService.delete(userId);

	}

	@PostMapping("/{userId}/changeMood/{moodName}")
	public void changeMood(@PathVariable Long userId, @PathVariable String moodName)
			throws UnnexpectedMoodNameException, NotFoundException {

		usersService.changeMoodOf(userId, moodName);
	}

	@GetMapping("/{userId}/isTrackRecommended")
	public Boolean isTrackRecommendedFor(@RequestBody Track track, @PathVariable Long userId)
			throws NotFoundException {

		return usersService.isTrackRecommendableForUser(track, userId);
	}

	@GetMapping("/{userId}/filterTracksRecommended")
	public List<Track> filterTracksRecommendedFor(@RequestBody List<Track> tracks,
			@PathVariable Long userId)
			throws NotFoundException {

		return usersService.filterRecommendableForUser(tracks, userId);
	}
}
