package net.christianemo.chiquimusic.users.model.external;

import lombok.Data;
import lombok.NonNull;

@Data
public class Track {

	private Long id;

	private Album album;

	private String songName;

	private Integer durationInSec;

	private String genre;
	
	private Float price;

	private String isBoring;
	
	private Boolean isNew;

	private Boolean isOld;

	private Boolean isShort;
	
}
