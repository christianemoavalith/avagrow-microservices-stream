package net.christianemo.chiquimusic.users.model.moods;

import net.christianemo.chiquimusic.users.model.Mood;
import net.christianemo.chiquimusic.users.model.external.Track;

public class AngryMood implements Mood {

	@Override
	public Boolean isRecomendable(Track track) {
		return track.getGenre().toLowerCase().contains("punk");
	}

}
