package net.christianemo.chiquimusic.users.model.moods;

import net.christianemo.chiquimusic.users.model.Mood;
import net.christianemo.chiquimusic.users.model.external.Track;

public class MelancholicMood implements Mood {

	@Override
	public Boolean isRecomendable(Track track) {
		return track.getIsOld();
	}

}
