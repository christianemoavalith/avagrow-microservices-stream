package net.christianemo.chiquimusic.users.viewmodels;

import lombok.Data;
import net.christianemo.chiquimusic.users.errors.UnnexpectedMoodNameException;
import net.christianemo.chiquimusic.users.model.Mood;
import net.christianemo.chiquimusic.users.model.User;

@Data
public class UserVM {

	private Long id;

	private String username;

	private String mood;

	public static UserVM of(User user) {
		var uvm = new UserVM();
		uvm.setId(user.getId());
		uvm.setUsername(user.getUsername());
		uvm.setMood(user.getMood().getClass().getSimpleName());
		return uvm;
	}

	public static User userOf(UserVM uservm) throws UnnexpectedMoodNameException {
		var user = new User(uservm.getUsername());
		user.setId(uservm.getId());
		user.setMood(Mood.createMoodFor(uservm.getMood()));
		return user;
	}
}
