package net.christianemo.chiquimusic.users.model;

import net.christianemo.chiquimusic.users.errors.NotFoundException;
import net.christianemo.chiquimusic.users.errors.UnnexpectedMoodNameException;
import net.christianemo.chiquimusic.users.model.external.Track;
import net.christianemo.chiquimusic.users.model.moods.AngryMood;
import net.christianemo.chiquimusic.users.model.moods.HappyMood;
import net.christianemo.chiquimusic.users.model.moods.MelancholicMood;

public interface Mood {

	public Boolean isRecomendable(Track track);

	public static Mood createMoodFor(String moodName) throws UnnexpectedMoodNameException {
		var lowercase = moodName.toLowerCase();

		switch (lowercase) {

			case "angry":
				return new AngryMood();

			case "melancholic":
				return new MelancholicMood();

			case "Happy":
				return new HappyMood();

			default:
				throw new UnnexpectedMoodNameException();

		}
	}

}
