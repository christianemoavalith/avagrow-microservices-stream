# avagrow-microservices-stream

Landing page for the project named "Stream" made for the #GrowWithUs challenge in Avalith

## Links

* [Old class diagram of the problem domain](diagrams/base-model.md)
* [Architecture of the intended microservices-based system](diagrams/msv-model.md)
  * Architecture is made by combinning...
    * [TRACKS class diagram](diagrams/track-msv.md)
    * [PLAYER class diagram](diagrams/player-msv.md)
    * [USERS class diagram](diagrams/users-msv.md)
    * [PLAYLIST class diagram](diagrams/playlist-msv.md)
    * [ACCOUNTING class diagram](diagrams/accounting-msv.md)
