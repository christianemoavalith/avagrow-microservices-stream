# PLAYLIST microservice

To see how this relates to other microservices, watch the [microservice model](msv-model.md).

## Class diagram

```plantuml
@startuml PLAYLIST
title PLAYLIST
skinparam nodesep 100

() USERS
() TRACKS

PlaylistController *-> PlaylistService : API for
PlaylistService ..> Playlist : Manages
PlaylistService ..> UsersClient : Uses
PlaylistService ..> TracksClient : Uses
UsersClient --> USERS
TracksClient --> TRACKS


class PlaylistController{
  - playlistService : PlaylistService
  + POST create(playlist) : Playlist 
  + GET read(id) : Playlist 
  + GET list() : List<Playlist> 
  + PUT update(playlist, id) : Playlist 
  + PATCH patch(playlist, id) : Playlist 
  + DELETE delete(id) : void 
  + POST createDefaultPlaylistFor(user: User) : Playlist
  + POST reccommendedSongsFor(user: User) : Playlist
}

class PlaylistService{
  - playlistsList // On memory, file or database
  + create(playlist, album) : Playlist 
  + read(id) : Playlist 
  + list() : List<Playlist> 
  + update(playlist, id) : Playlist 
  + delete(id) : void 
  + createDefaultPlaylistFor(user: User) : Playlist
  + reccommendedSongsFor(user: User) : Playlist
}


class Playlist{
  - id : integer
  - name : string
  - userId : integer
  - tracks : List<Track>
}


class UsersClient{
  - connectionData
  + create(user) : User 
  + read(id) : User 
  + list() : List<User> 
  + update(user, id) : User 
  + delete(id) : void 
  + changeMoodOf(user, mood) : void
  + wantedGenreOf(user) : string
}

class TracksClient{
  - connectionData
  + createAlbum(album) : Album 
  + createTrack(track, album) : Track 
  + readAlbum(id) : Album 
  + readTrack(id) : Track 
  + listAlbums() : List<Album> 
  + listTracks() : List<Track> 
  + updateAlbum(album, id) : Album 
  + updateTrack(track, id) : Track 
  + deleteAlbum(id) : void
  + deleteTrack(id) : void 
  + addTrackToAlbum(track,album) : void
  + getAmmountOfTracksOfAlbum(album) : integer
  + isTrackNew(trackId) : bool
  + isTrackShort(trackId) : bool
  + isTrackOld(trackId) : bool
  + isTrackBoring(trackId) : bool
}


package outertypes{

  Track *-- Album

  class Track {
    - id : integer
    - album : Album
    - songName : string
    - durationInSec : integer
    - genre : string
    - price : float
    + isNew() : bool
    + isShort() : bool
    + isOld() : bool
    + isBoring() : bool
  }

  class Album <<simplified>> {
    - id : integer
    - producer : string
    - artist : string
    - members : integer
    - releaseYear : integer
  }
}


@enduml
```
