# Microservice model

The pourpose of this document is to show a possible solution that models the problem in UML.

For this document, each component can be interpreted as a microservice.

## Architecture

```plantuml
@startuml Architecture
title Architecture

component TRACKS
component USERS
component PLAYLIST
component PLAYER
component ACCOUNTINGS

PLAYER ..> USERS 
PLAYER ..> PLAYLIST 
PLAYER ...> ACCOUNTINGS
PLAYLIST ..> TRACKS 
PLAYLIST .> ACCOUNTINGS 
USERS <. PLAYLIST
USERS <.. ACCOUNTINGS 
ACCOUNTINGS .> TRACKS 

@enduml
```

Those components are intended to deliver these responsabilities:
(click to see more details)

* [PLAYER](player-msv.md)
  * Plays the songs of a playlist
  * Lets users to log in a user at a time
  * Needs to know users' mood and notifies it to USERS
  * Can show the PLAYLIST's reccomended song for current user
  * Lets users be able to manage their ACCONTINGS
* [USERS](users-msv.md)
  * A repository of users
  * Each user can manage it's own mood (State/Strategy)
* [PLAYLIST](playlist-msv.md)
  * Manages the songs the player can access to.
    * It's aware of user's preferences.
    * It's aware of user's accountings.
  * Can reccomend songs
  * Can make a default playlist using the available funny songs.
* [ACCOUNTINGS](accounting-msv.md)
  * Services can query which songs are available for a given user
  * There are two types of accountings
    * Prepaid ones:
      * The user charges credit when he wants to.
      * The needed credit is debited each time the user buys a song.
      * The accounting keeps register of bought songs.
    * Monthly:
      * The user pays monthly to access all the songs he wants to.
      * The accounting keeps register about payments and expiration dates.
* [TRACK](track-msv.md)
  * Repository of tracks
  * Tracks are grouped in Albums
  * Each track is selfaware about being new, old, short and/or boring
