# PLAYER microservice

To see how this relates to other microservices, watch the [microservice model](msv-model.md).

## Class diagram

```plantuml
@startuml PLAYER
title PLAYER
skinparam nodesep 100

() view
() USERS
() PLAYLIST
() ACCOUNTINGS

view -> PlayerController
PlayerController *-> PlayerService : API for
PlayerService ..> PlaylistClient : consumes
PlayerService ..> AccountingClient : consumes
PlayerService ..> UsersClient : consumes
PlayerService ..> Player : manages
PlaylistClient ..> PLAYLIST
AccountingClient ..> ACCOUNTINGS
UsersClient ..> USERS

class PlayerController{
  - playerService : PlayerService
  + GET play() : string??
  + POST login(loginData) : boolean
  + POST logout(loginData) : boolean
  + POST createAccount() : void
  + GET getAccountingSettings(settings) : boolean
  + POST setAccountingSettings(settings) : boolean
  + POST setMood(mood) : void
  + GET showPlaylist() : ?
  + POST pay(concept, ammount) : ?
  + GET playDefaultPlaylist() : string??
  + GET playReccomendations() : string??
}

class PlayerService{
  - playerList : List<Player>
  + play() : string
  + login(loginData) : boolean
  + logout(loginData) : boolean
  + createAccount() : void
  + getAccountingSettings(settings) : boolean
  + setAccountingSettings(settings) : boolean
  + setMood(mood) : void
  + showPlaylist() : ?
  + pay(concept, ammount) : ?
}

class Player{
  - user
  - account
  - playlist
}

class PlaylistClient{
  - connectionData
  + create(playlist, album) : Playlist 
  + read(id) : Playlist 
  + list() : List<Playlist> 
  + update(playlist, id) : Playlist 
  + delete(id) : void 
  + createDefaultPlaylistFor(userId) : Playlist
  + getReccommendedSongsFor(userId) : Playlist
}

class UsersClient{
  - connectionData
  + create(user) : User 
  + read(id) : User 
  + list() : List<User> 
  + update(user, id) : User 
  + delete(id) : void 
  + changeMoodOf(user, mood) : void
  + wantedGenreOf(user) : string
}


class AccountingClient{
  - connectionData
  + create(account) : Account 
  + read(id) : Account 
  + list() : List<Account> 
  + update(account, id) : Account 
  + delete(id) : void 
  + canAccessTo(account, trackId) : boolean
  + buy(account,trackId) : void
  + chargeCredit(account, credit) : void
  + payMonth(account) : void
}


package outertypes{

  Track *-- Album

  class Track {
    - id : integer
    - album : Album
    - songName : string
    - durationInSec : integer
    - genre : string
    - price : float
    + isNew() : bool
    + isShort() : bool
    + isOld() : bool
    + isBoring() : bool
  }

  class Album <<simplified>> {
    - id : integer
    - producer : string
    - artist : string
    - members : integer
    - releaseYear : integer
  }
}


@enduml
```
