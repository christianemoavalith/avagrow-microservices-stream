# PLAYER microservice

To see how this relates to other microservices, watch the [microservice model](msv-model.md).

## Class diagram

```plantuml
@startuml USERS
title USERS
skinparam nodesep 100

UserController *-> UserService : API for
UserService ..> User : Manages
User -left-> Mood
Mood <|.. Angry
Mood <|.. Melancholic
Mood <|.. Happy

class UserController{
  - userService : UserService
  + POST create(user) : User 
  + GET read(id) : User 
  + GET list() : List<User> 
  + PUT update(user, id) : User 
  + PATCH patch(user, id) : User 
  + DELETE delete(id) : void 
  + GET wantedGenreOf(user) : string
  + POST changeMoodOf(user, mood) : void
  + GET isReccomendableFor(user, track) : boolean
}

class UserService{
  - tracksList // On memory, file or database
  + create(user) : User 
  + read(id) : User 
  + list() : List<User> 
  + update(user, id) : User 
  + delete(id) : void 
  + changeMoodOf(user, mood) : void
  + isReccomendableFor(user, track) : boolean
}

class User{
  - id : integer
  - username : string
  - mood : Mood
}

interface Mood{
  + isReccomendable(track) : boolean
}

class Angry {
  +^ isReccomendable(track) : boolean
}

class Melancholic {
  +^ isReccomendable(track) : boolean
}

class Happy {
  +^ isReccomendable(track) : boolean
}

package outertypes{

  Track *-- Album

  class Track {
    - id : integer
    - album : Album
    - songName : string
    - durationInSec : integer
    - genre : string
    - price : float
    + isNew() : bool
    + isShort() : bool
    + isOld() : bool
    + isBoring() : bool
  }

  class Album <<simplified>> {
    - id : integer
    - producer : string
    - artist : string
    - members : integer
    - releaseYear : integer
  }
}

@enduml
```
