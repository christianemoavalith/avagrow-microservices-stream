# TRACKS microservice

To see how this relates to other microservices, watch the [microservice model](msv-model.md).

For this one I'm evualuationg two posibilities

1. Have Album being a hard entity that groups Track objects with related data.
   * Each Album object holds a list of Track objects.
2. Making Track the harder entity, Album only holds a handful of data.
   * Tracks are not grouped, to see what tracks belong to a single album, we need a service who searches what tracks share the same value in the album attribute.

**We finally decided to go for a merged revision of those two aproaches** where we can take adventages of both and assume the complexities involved.

## Class diagram (merged revision)

```plantuml
@startuml TRACKS
title TRACKS
skinparam nodesep 100

AlbumController --> AlbumService : API for
TrackController --> TrackService : API for
AlbumService <-> TrackService : Can query
AlbumService --> Album : Controls
TrackService --> Track : Controls
TrackService ..> Album : Knows
Album "1" *-* "1..N" Track : Has


class AlbumController{
  - albumService : AlbumService
  + {POST} create(album) : Track 
  + {GET} read(id) : Track 
  + {GET} list() : List<Album> 
  + {PUT} update(track, id) : Album 
  + {PATCH} patch(track, id) : Album 
  + {DELETE} delete(id) : void 
  + {GET} addTrackTo(track,album) : void
  + {GET} getAmmountOfTracksOf(album) : integer
}

class TrackController{
  - trackService : TrackService
  + {POST} create(track, album) : Track 
  + {GET} read(id) : Track 
  + {GET} list() : List<Track> 
  + {PUT} update(track, id) : Track 
  + {PATCH} patch(track, id) : Track 
  + {DELETE} delete(id) : void 
  + {GET} isNew(trackId) : bool
  + {GET} isShort(trackId) : bool
  + {GET} isOld(trackId) : bool
  + {GET} isBoring(trackId) : bool
}

class AlbumService{
  - albumList // On memory, file or database
  + create(album) : Album 
  + read(id) : Album 
  + list() : List<Album> 
  + update(album, id) : Album 
  + delete(id) : void
  + addTrackTo(track,album) : void
  + getAmmountOfTracksOf(album) : integer
}

class TrackService{
  - tracksList // On memory, file or database
  + create(track, album) : Track 
  + read(id) : Track 
  + list() : List<Track> 
  + update(track, id) : Track 
  + delete(id) : void 
  + isNew(trackId) : bool
  + isShort(trackId) : bool
  + isOld(trackId) : bool
  + isBoring(trackId) : bool
}

class Track {
  - id : integer
  - album : Album
  - songName : string
  - durationInSec : integer
  - genre : string
  - price : float
  + isNew() : bool
  + isShort() : bool
  + isOld() : bool
  + isBoring() : bool
}


class Album {
  - id : integer
  - producer : string
  - artist : string
  - members : integer
  - releaseYear : integer
  - tracks : List<Track>
  + add(track) : void
  + getAmmountOfTracks() : integer
}
@enduml
```
