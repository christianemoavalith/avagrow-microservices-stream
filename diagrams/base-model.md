# Base model

This base model was thought without taking account of the implementation nor architecture of the system.

Its pourpose is to show off a possible solution that models the problem in UML.

```plantuml
@startuml Primitive model

title BASE MODEL   

Library o--> Track  
Library o--> Album
Library <.. User : Can access
Track <.  MediaPlayer : Plays
Album - Track : Has
User <-- MediaPlayer : Has
Track <. User : Buys and owes

note "Needed constructors,\ngetters and setters\nare taken for granted." as N1

class Library <<Singleton>> {
    - tracks : List<Track>
    - albums : List<Album>
    + addTrack(...)
    + addAlbum(...)
}

class Track {
    - album : Album
    - songName : string
    - durationInSec : int
    - category : string
    - price : float
    + isNew() : bool
    + isShort() : bool
    + isOld() : bool
    + isBoring() : bool
}

class Album {
    - producer : string
    - artist : string
    - members : int
    - releaseYear : int
    - tracks : List<Track>
    + addTrack(...)
    + getAmmountOfTracks() : int
}

class MediaPlayer{
    - user : User
    - defaultPlaylist : List<Track>
    + play(track : Track) : void
    + recommendTrack() : Track
    + updateDefaultPlalist() : void
}

class User {
    - mood : string
    - credit : float
    - boughtTracks : List<Track>
    + changeMood(mood : string) : void
    + chargeCredit(amount : float) : void
    + buyATrack(track : Track) : bool
}

@enduml
```

## Thougts and logs

* Many Track's attributes can be wraped up in a class
  * Decided to add Album class
* It is not clear where the tracks are stored
  * Decided to add Library class
