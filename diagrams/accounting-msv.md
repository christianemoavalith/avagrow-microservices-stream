# ACCOUNTING microservice

To see how this relates to other microservices, watch the [microservice model](msv-model.md).

The interface Type is involved in 2 common design patterns:

* the method getTypeFor(typename) is a factory method
* the class Type itself is designed with the intention of using its subclasses as strategies for the accounts.

## Class diagram

```plantuml
@startuml ACCOUNTING
title ACCOUNTING
skinparam nodesep 50

() USERS

AccountingController -> AccountingService : API for
AccountingService ..> UserClient : Uses
AccountingService ..> Account : Manages
Account *-> Type
Account *-left-> User
Account <..> Prepaid #88a
Account <..> Monthly #88a
Type ^-- Prepaid
Type ^-- Monthly
AccountingController .[hidden]. Type
UserClient --> USERS : Consumes

class AccountingController{
  - accountingService : AccountingService
  + {POST} create(account) : Account 
  + {GET} read(id) : Account 
  + {GET} list() : List<Account> 
  + {PUT} update(account, id) : Account 
  + {PATCH} patch(account, id) : Account 
  + {DELETE} delete(id) : void 
  + {GET} canAccessTo(account, track) : boolean
  + {GET} getListOfAvailablesFor(account) : List<Track>
  + {POST} buy(account,track) : buy
  + {POST} chargeCredit(account, credit) : void
  + {POST} payMonth(account) : void
}

class AccountingService{
  - accountsList // On memory, file or database
  + create(account) : Account 
  + read(id) : Account 
  + list() : List<Account> 
  + update(account, id) : Account 
  + delete(id) : void 
  + canAccessTo(account, track) : boolean
  + buy(account,track) : void
  + chargeCredit(account, credit) : void
  + payMonth(account) : void
}


class User{
  - id : integer
  - username : string
  - mood : Mood
}

class Account{
  - id : integer
  - user : User
  - type : Type
  - tracksOwned : List<Track>
  - credit : float
  + canAccessTo(track) : boolean
  + buy(track) : void
  + chargeCredit(credit) : void
  + payMonth() : void
}

interface Type {
  + {static} getType(typename) : Type  // <--factory method
  + canAccess(track) : boolean // return true 
  + purchaseSong(track) : boolean
  + payMonthOf(account) : void
}

class Monthly {
  +^ canAccess(account, track) : boolean // return true 
  +^ purchaseSong(account, track) : boolean // do nothing
  +^ payMonthOf(account) : void
}

class Prepaid {
  +^ canAccess(account, track) : boolean
  +^ purchaseSong(account, track) : boolean
  +^ payMonthOf(account) : void // do nothing
}

class UserClient{
  - connectionData
  + create(user) : User 
  + read(id) : User 
  + list() : List<User> 
  + update(user, id) : User 
  + delete(id) : void 
  + changeMoodOf(user, mood) : void
  + wantedGenreOf(user) : string
}

package outertypes{

  Track *-- Album

  class Track {
    - id : integer
    - album : Album
    - songName : string
    - durationInSec : integer
    - genre : string
    - price : float
    + isNew() : bool
    + isShort() : bool
    + isOld() : bool
    + isBoring() : bool
  }

  class Album <<simplified>> {
    - id : integer
    - producer : string
    - artist : string
    - members : integer
    - releaseYear : integer
  }
}

@enduml
```
