package net.christianemo.chiquimusic.tracks.model;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;
import lombok.NonNull;

@Data
public class Album {

	private Long id;

	private String producer;
	
	@NonNull
	private String artist;

	private Integer members;

	private Integer releaseYear;

	private List<Track> tracks = new ArrayList<>();

	public Integer getAmmountOfTracks() {
		return tracks.size();
	}
}
