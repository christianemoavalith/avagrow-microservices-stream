package net.christianemo.chiquimusic.tracks.model.genres;

import net.christianemo.chiquimusic.tracks.model.Genre;
import net.christianemo.chiquimusic.tracks.model.Track;

public class PunkGenre implements Genre {

	private static final int DURATION_LIMIT = 2*60;

	@Override
	public boolean isBoring(Track track) {
		return track.getDurationInSec() > DURATION_LIMIT;
	}

}
