package net.christianemo.chiquimusic.tracks.model.genres;

import net.christianemo.chiquimusic.tracks.model.Genre;
import net.christianemo.chiquimusic.tracks.model.Track;

public class RockGenre implements Genre {

	private static final int TRACKS_LIMIT = 9;

	@Override
	public boolean isBoring(Track track) {
		return track.getAlbum().getAmmountOfTracks() < TRACKS_LIMIT;
	}

}
