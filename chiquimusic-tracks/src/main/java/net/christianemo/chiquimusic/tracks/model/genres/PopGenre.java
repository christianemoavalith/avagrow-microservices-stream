package net.christianemo.chiquimusic.tracks.model.genres;

import net.christianemo.chiquimusic.tracks.model.Genre;
import net.christianemo.chiquimusic.tracks.model.Track;

public class PopGenre implements Genre {

	@Override
	public boolean isBoring(Track track) {
		return track.getAlbum().getMembers() == 1; //es solista
	}

}
