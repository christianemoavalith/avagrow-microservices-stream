package net.christianemo.chiquimusic.tracks.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import net.christianemo.chiquimusic.tracks.exceptions.NotFoundException;
import net.christianemo.chiquimusic.tracks.model.Track;
import net.christianemo.chiquimusic.tracks.services.TracksService;

@RestController
@RequestMapping("/api/tracks")
public class TracksController {

	@Autowired
	private TracksService tracksServ;

	@PostMapping
	public Track create(Track track) {
		return tracksServ.create(track);
	}

	@GetMapping("/{id}")
	public Track read(@PathVariable Long id) throws NotFoundException {
		return tracksServ.read(id);
	}

	@GetMapping
	public List<Track> list() {
		return tracksServ.list();
	}

	@PutMapping("/{id}")
	public Track update(@RequestBody Track track, @PathVariable Long id) throws NotFoundException {
		return tracksServ.update(track, id);
	}

	@DeleteMapping("/{id}")
	public Boolean delete(@PathVariable Long id) throws NotFoundException {
		return tracksServ.delete(id);
	}

	@GetMapping("/isnew/{id}")
	public Boolean isNew(@PathVariable Long id) throws NotFoundException {
		return tracksServ.isNew(id);
	}

	@GetMapping("/isold/{id}")
	public Boolean isOld(@PathVariable Long id) throws NotFoundException {
		return tracksServ.isOld(id);
	}

	@GetMapping("/isshort/{id}")
	public Boolean isShort(@PathVariable Long id) throws NotFoundException {
		return tracksServ.isShort(id);
	}

}
