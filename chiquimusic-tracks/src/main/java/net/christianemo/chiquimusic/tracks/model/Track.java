package net.christianemo.chiquimusic.tracks.model;

import lombok.Data;
import lombok.NonNull;

@Data
public class Track {

	private static final Integer SHORT_LIMIT = 120;
	
	private static final Integer NEW_LIMIT = 2015;
	
	private static final Integer OLD_LIMIT = 2000;

	private Long id;

	@NonNull
	private final Album album;

	@NonNull
	private String songName;

	private Integer durationInSec;

	private Genre genre;

	private Float price;

	public Boolean isNew() {
		return (album.getReleaseYear() > NEW_LIMIT);
	}

	public Boolean isOld() {
		return (album.getReleaseYear() < OLD_LIMIT);
	}

	public Boolean isShort() {
		return (this.durationInSec < SHORT_LIMIT);
	}

	public Boolean isBoring() {
		return genre.isBoring(this);
	}
}
