package net.christianemo.chiquimusic.tracks;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class ChiquimusicTracksApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChiquimusicTracksApplication.class, args);
	}

}
