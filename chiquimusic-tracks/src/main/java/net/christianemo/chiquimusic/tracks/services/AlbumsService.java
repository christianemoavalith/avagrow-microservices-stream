package net.christianemo.chiquimusic.tracks.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import lombok.NonNull;
import net.christianemo.chiquimusic.tracks.exceptions.NotFoundException;
import net.christianemo.chiquimusic.tracks.model.Album;

@Service
public class AlbumsService {

	// simulates database
	private List<Album> albums = new ArrayList<>();

	public Album create(Album album) {
		Long max = albums
				.stream()
				.mapToLong(a -> a.getId())
				.max()
				.orElse(0);
		Long nextId = max + 1; // First id will always be 1
		album.setId(nextId);
		albums.add(album);
		return album;
	}

	public Album read(Long id) throws NotFoundException {
		return findInAlbumsById(id);
	}

	public List<Album> list() {
		return new ArrayList<>(this.albums);
		// returns a new list so that encapsulation doesn't get violated.
	}

	public Album update(Album album, Long id) throws NotFoundException {
		if (album.getId().equals(id)) {
			Integer index = albums.indexOf(findInAlbumsById(id));

			if (index >= 0) {
				albums.set(index, album);
				return album;
			}
		}

		return null;
	}

	public void delete(Long id) throws NotFoundException {
		albums.remove(findInAlbumsById(id));
	}

	private Album findInAlbumsById(Long albumId) throws NotFoundException {
		return albums.stream()
				.filter(a -> a.getId().equals(albumId))
				.findFirst()
				.orElseThrow(() -> new NotFoundException());
	}

	public Album getAlbumEqualsTo(@NonNull Album album) throws NotFoundException {
		return albums.stream()
				.filter(a -> a.equals(album))
				.findFirst()
				.orElseThrow(()-> new NotFoundException());
	}

}
