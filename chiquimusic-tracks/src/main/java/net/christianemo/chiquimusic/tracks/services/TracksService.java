package net.christianemo.chiquimusic.tracks.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.christianemo.chiquimusic.tracks.exceptions.NotFoundException;
import net.christianemo.chiquimusic.tracks.model.Track;

@Service
public class TracksService {

	@Autowired
	private AlbumsService albumsService;

	// simulates database
	private List<Track> tracks = new ArrayList<>();

	public Track create(Track track) {
		Long max = tracks
				.stream()
				.mapToLong(t -> t.getId())
				.max()
				.orElse(0);
		Long nextId = max + 1; // First id will always be 1
		track.setId(nextId);

		var trackAlbum = track.getAlbum();
		try {
			var storedAlbum = albumsService.getAlbumEqualsTo(trackAlbum);
			storedAlbum.getTracks().add(track);
		} catch (NotFoundException e) {
			if (!(trackAlbum.getTracks().contains(track))) {
				trackAlbum.getTracks().add(track);
			}
			albumsService.create(trackAlbum);
		}

		tracks.add(track);
		return track;
	}

	public Track read(Long id) throws NotFoundException {
		return findInTracksById(id);
	}

	public List<Track> list() {
		return new ArrayList<>(this.tracks);
		// returns a new list so that encapsulation doesn't get violated.
	}

	public Track update(Track track, Long id) throws NotFoundException {
		if (track.getId().equals(id)) {
			Integer index = tracks.indexOf(findInTracksById(id));

			if (index >= 0) {
				tracks.set(index, track);
				return track;
			}
		}

		return null;
	}

	public Boolean delete(Long id) throws NotFoundException {
		Track found = findInTracksById(id);

		if (found != null) {
			return tracks.remove(found);
		}

		return false;
	}

	public Boolean isNew(Long trackId) throws NotFoundException {
		Track track = findInTracksById(trackId);

		if (track != null) {
			return track.isNew();
		}

		return null;
	}

	public Boolean isOld(Long trackId) throws NotFoundException {
		Track track = findInTracksById(trackId);

		if (track != null) {
			return track.isOld();
		}

		return null;
	}

	public Boolean isShort(Long trackId) throws NotFoundException {
		Track track = findInTracksById(trackId);

		if (track != null) {
			return track.isShort();
		}

		throw new NotFoundException();
	}

	public Boolean isBoring(Long id) throws NotFoundException {
		Track track = findInTracksById(id);
		if (track != null) {
			return track.isBoring();
		}
		throw new NotFoundException();
	}

	private Track findInTracksById(Long trackId) throws NotFoundException {
		return tracks.stream()
				.filter(t -> t.getId().equals(trackId))
				.findFirst()
				.orElseThrow(() -> new NotFoundException());
	}

	public Boolean isReady() {
		return albumsService != null;
	}

}
