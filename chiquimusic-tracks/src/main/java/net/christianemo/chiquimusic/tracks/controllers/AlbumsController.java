package net.christianemo.chiquimusic.tracks.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import net.christianemo.chiquimusic.tracks.exceptions.NotFoundException;
import net.christianemo.chiquimusic.tracks.model.Album;
import net.christianemo.chiquimusic.tracks.services.AlbumsService;

@RestController
@RequestMapping("/api/albums")
public class AlbumsController {

	@Autowired
	private AlbumsService albumsServ;

	@PostMapping
	public Album create(Album album) {
		return albumsServ.create(album);
	}

	@GetMapping("/{id}")
	public Album read(@PathVariable Long id) throws NotFoundException {
		return albumsServ.read(id);
	}

	@GetMapping
	public List<Album> list() {
		return albumsServ.list();
	}

	@PutMapping("/{id}")
	public Album update(@RequestBody Album album, @PathVariable Long id) throws NotFoundException {
		return albumsServ.update(album, id);
	}

	@DeleteMapping("/{id}")
	public void delete(@PathVariable Long id) throws NotFoundException {
		albumsServ.delete(id);
	}

}
