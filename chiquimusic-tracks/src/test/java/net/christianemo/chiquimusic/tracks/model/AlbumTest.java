package net.christianemo.chiquimusic.tracks.model;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

class AlbumTest {

	@Test
	void testGetAmmountOfTracks() {
		Album a1 = new Album("Artist1");
		a1.getTracks().add(new Track(a1, "Track1"));
		a1.getTracks().add(new Track(a1, "Track2"));
		a1.getTracks().add(new Track(a1, "Track3"));
		a1.getTracks().add(new Track(a1, "Track4"));
		
		assertEquals(a1.getAmmountOfTracks(), 4);
	}

}
