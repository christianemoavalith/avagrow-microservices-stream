package net.christianemo.chiquimusic.tracks.services;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import javax.validation.constraints.AssertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import net.christianemo.chiquimusic.tracks.exceptions.NotFoundException;
import net.christianemo.chiquimusic.tracks.model.Album;
import net.christianemo.chiquimusic.tracks.model.Track;
import net.christianemo.chiquimusic.tracks.model.genres.PopGenre;
import net.christianemo.chiquimusic.tracks.model.genres.PunkGenre;
import net.christianemo.chiquimusic.tracks.model.genres.ReggetonGenre;
import net.christianemo.chiquimusic.tracks.model.genres.RockGenre;

@SpringBootTest
class TracksServiceTest {

	@Autowired
	private TracksService tracksService;

	private Track track1;

	private Track track2;

	private Track track3;

	private Track track4;

	@BeforeEach
	void init() {
		track1 = new Track(new Album("Alb1"), "Trk1");
		track2 = new Track(new Album("Alb2"), "Trk2");
		track3 = new Track(new Album("Alb3"), "Trk3");
		track4 = new Track(new Album("Alb4"), "Trk4");
	}

	@Test
	void testAvailability() {
		assertTrue(tracksService.isReady());
	}

	@Test
	void testCreateAndRead() throws NotFoundException {
		var created = tracksService.create(track1);
		var fetched = tracksService.read(created.getId());
		assertEquals(created, fetched);
	}

	@Test
	void testCreateIncrementalId() {
		var created1 = tracksService.create(track1);
		var created2 = tracksService.create(track2);
		assertEquals(created1.getId() + 1, created2.getId());
	}

	@Test
	void testCreateAndList() {
		var currentsize = tracksService.list().size();

		tracksService.create(track1);
		tracksService.create(track2);

		assertEquals(tracksService.list().size(), currentsize + 2);
	}

	@Test
	void testCreateUpdateRead() throws NotFoundException {
		var newName = "La Calesita";

		var created = tracksService.create(track1);
		var fetched = tracksService.read(created.getId());
		fetched.setSongName(newName);

		var edited = tracksService.update(fetched, fetched.getId());
		var read = tracksService.read(fetched.getId()); // This time id is not supposed to change
		assertEquals(edited, read);
	}

	@Test
	void testDelete() throws NotFoundException {
		var created = tracksService.create(track1);
		tracksService.delete(created.getId());

		assertThrows(
				NotFoundException.class,
				() -> tracksService.read(created.getId()));
	}

	@Test
	void testIsNew() throws NotFoundException {
		var yearOk = 2016; // new is greater than 2015
		track1.getAlbum().setReleaseYear(yearOk);
		var created1 = tracksService.create(track1);

		var yearFail = 2015; // new is greater than 2015
		track2.getAlbum().setReleaseYear(yearFail);
		var created2 = tracksService.create(track2);

		assertAll(
				() -> assertTrue(tracksService.isNew(created1.getId())),
				() -> assertFalse(tracksService.isNew(created2.getId())));
	}

	@Test
	void testIsOld() throws NotFoundException {
		var yearOk = 1999;// old is before 2000
		track1.getAlbum().setReleaseYear(yearOk);
		var created1 = tracksService.create(track1);

		var yearFail = 2000;// old is before 2000
		track2.getAlbum().setReleaseYear(yearFail);
		var created2 = tracksService.create(track2);

		assertAll(
				() -> assertTrue(tracksService.isOld(created1.getId())),
				() -> assertFalse(tracksService.isOld(created2.getId())));
	}

	@Test
	void testIsShort() {
		var durationOk = 119; // short is less than 120
		track1.setDurationInSec(durationOk);
		var created1 = tracksService.create(track1);

		var durationFail = 120; // short is less than 120
		track2.setDurationInSec(durationFail);
		var created2 = tracksService.create(track2);

		assertAll(
				() -> assertTrue(tracksService.isShort(created1.getId())),
				() -> assertFalse(tracksService.isShort(created2.getId())));
	}

	@Test
	void testIsBoring_Punk() throws NotFoundException {
		track1.setGenre(new PunkGenre());
		track1.setDurationInSec(121);
		var created = tracksService.create(track1);

		assertTrue(tracksService.isBoring(created.getId()));
	}

	@Test
	void testIsBoring_Reggeton() throws NotFoundException {
		track1.setGenre(new ReggetonGenre());
		track1.getAlbum().setReleaseYear(1999);
		var created = tracksService.create(track1);

		assertTrue(tracksService.isBoring(created.getId()));
	}

	@Test
	void testIsBoring_Rock() throws NotFoundException {
		var album1 = track1.getAlbum();
		track1.setGenre(new RockGenre());
		
		//is boring if has less than 9 tracks
		album1.getTracks().add(new Track(album1, "Trk2"));
		album1.getTracks().add(new Track(album1, "Trk3"));
		album1.getTracks().add(new Track(album1, "Trk4"));
		album1.getTracks().add(new Track(album1, "Trk5"));
		album1.getTracks().add(new Track(album1, "Trk6"));
		album1.getTracks().add(new Track(album1, "Trk7"));
		album1.getTracks().add(new Track(album1, "Trk8"));

		var created = tracksService.create(track1);

		assertTrue(tracksService.isBoring(created.getId()));
	}

	@Test
	void testIsBoring_Pop() throws NotFoundException {
		track1.setGenre(new PopGenre());
		track1.getAlbum().setMembers(1);

		var created = tracksService.create(track1);

		assertTrue(tracksService.isBoring(created.getId()));
	}

}
