package net.christianemo.chiquimusic.tracks.model;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import net.christianemo.chiquimusic.tracks.model.genres.PopGenre;
import net.christianemo.chiquimusic.tracks.model.genres.PunkGenre;
import net.christianemo.chiquimusic.tracks.model.genres.ReggetonGenre;
import net.christianemo.chiquimusic.tracks.model.genres.RockGenre;

class TrackTest {

	@Test
	void testIsNew() {
		Album a1 = new Album("Artist");
		Track t1 = new Track(a1, "Track1");

		// Limit year was originally expected to be 2015, so...
		t1.getAlbum().setReleaseYear(2016);

		assertTrue(t1.isNew());
	}

	@Test
	void testIsOld() {
		Album a1 = new Album("Artist");
		Track t1 = new Track(a1, "Track1");

		// Limit year was originally expected to be 2000, so...
		t1.getAlbum().setReleaseYear(1999);

		assertTrue(t1.isOld());
	}

	@Test
	void testIsShort() {
		Album a1 = new Album("Artist");
		Track t1 = new Track(a1, "Track1");

		// Less than 120 secconds is expected to be "short", so...
		t1.setDurationInSec(119);
		;

		assertTrue(t1.isShort());
	}

	@Test
	void testIsBoring_Punk() {
		var track1 = new Track(new Album("Pepe"), "Track1");
		track1.setGenre(new PunkGenre());
		track1.setDurationInSec(121);

		assertTrue(track1.isBoring());
	}

	@Test
	void testIsBoring_Reggeton() {
		var track1 = new Track(new Album("Pepe"), "Track1");
		track1.setGenre(new ReggetonGenre());
		track1.getAlbum().setReleaseYear(1999);

		assertTrue(track1.isBoring());
	}

	@Test
	void testIsBoring_Rock() {
		var track1 = new Track(new Album("Pepe"), "Trk1");
		track1.setGenre(new RockGenre());
		track1.getAlbum().getTracks().add(new Track(track1.getAlbum(), "Trk2"));
		track1.getAlbum().getTracks().add(new Track(track1.getAlbum(), "Trk3"));
		track1.getAlbum().getTracks().add(new Track(track1.getAlbum(), "Trk4"));
		track1.getAlbum().getTracks().add(new Track(track1.getAlbum(), "Trk5"));
		track1.getAlbum().getTracks().add(new Track(track1.getAlbum(), "Trk6"));
		track1.getAlbum().getTracks().add(new Track(track1.getAlbum(), "Trk7"));
		track1.getAlbum().getTracks().add(new Track(track1.getAlbum(), "Trk8"));
		track1.getAlbum().getTracks().add(new Track(track1.getAlbum(), "Trk9"));

		assertTrue(track1.isBoring());
	}

	@Test
	void testIsBoring_Pop() {
		var track1 = new Track(new Album("Pepe"), "Track1");
		track1.setGenre(new PopGenre());
		track1.getAlbum().setMembers(1);

		assertTrue(track1.isBoring());
	}

}
