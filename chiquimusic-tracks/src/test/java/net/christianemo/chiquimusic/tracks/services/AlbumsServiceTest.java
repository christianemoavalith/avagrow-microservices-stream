package net.christianemo.chiquimusic.tracks.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.stereotype.Service;

import net.christianemo.chiquimusic.tracks.exceptions.NotFoundException;
import net.christianemo.chiquimusic.tracks.model.Album;

@Service
class AlbumsServiceTest {

	/**
	 * The first element of each of this project's repositories are always 1L
	 */
	private static final long FIRST_ELEMENT = 1L;

	@Test
	void testCreateAndReadAlbum() throws Exception {
		var albumService = new AlbumsService();
		var album = new Album("Pepe");

		albumService.create(album);

		assertTrue(albumService.read(FIRST_ELEMENT).equals(album));
	}

	@Test
	void testCreateSetIdAndReadAlbum_shouldFail() throws Exception {
		var albumService = new AlbumsService();
		var album = new Album("Pepe");

		var random = (long) (((Math.random() * 100) % 100) + 2); // 0 and 1 are not useful
		album.setId(random);
		albumService.create(album);

		assertThrows(
				NotFoundException.class,
				() -> albumService.read(random));
	}

	@Test
	void testCreateAndList() {
		var albumService = new AlbumsService();
		List<Album> list = new ArrayList<Album>();

		list.add(new Album("Tino"));
		list.add(new Album("Milo"));
		list.add(new Album("Rita"));
		list.add(new Album("Alez"));

		list.forEach(a -> albumService.create(a));

		assertEquals(albumService.list(), list);
	}

	@Test
	void testCreateUpdateAndRead() throws NotFoundException {
		var albumService = new AlbumsService();
		var album1 = new Album("Hector");

		albumService.create(album1);

		var album2 = albumService.read(FIRST_ELEMENT);
		album2.setArtist("Tito");
		albumService.update(album2, FIRST_ELEMENT);

		var album3 = albumService.read(FIRST_ELEMENT);
		assertEquals(album2, album3);
	}

	@Test()
	void testCreateAndDelete() throws NotFoundException {
		var albumService = new AlbumsService();

		var album1 = new Album("El pepe");

		albumService.create(album1);
		albumService.delete(FIRST_ELEMENT);

		assertThrows(
				NotFoundException.class,
				() -> albumService.read(FIRST_ELEMENT));
	}

}
